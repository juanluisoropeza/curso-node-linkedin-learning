const fs = require('fs');
const http = require('http');

function leerArchivo() {
    //fs.readFileSync("./archivos/original.txt", "utf8");
    const streamLectura = fs.createReadStream( './archivos/original.txt', {
        encoding: 'utf8'
    });
}

http.createServer(function(req, res) {
    for (let i = 0; i < 5000; i++) {
        leerArchivo();
    }
    res.write("Hola mundo");
    res.end();
}).listen(8080);