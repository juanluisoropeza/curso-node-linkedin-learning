const fs = require('fs');
const util = require('util');

/*

fs.writeFile('./archivos/archivo.txt', '123456789', () => {
    console.log('Listo!');
});

es lo mismo que esta abajo pero con promesas para captar posibles errores

*/

const writeFilePromesa = util.promisify(fs.writeFile);

writeFilePromesa('./archivos/archivo.asdsdf', '123456789')
.then( () => {
    console.log('Listo!');
})
.catch( () => {
    console.log('Error!');
});