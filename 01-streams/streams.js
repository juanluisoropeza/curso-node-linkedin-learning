// invocamos al file system
const fs = require('fs');

console.time('Tiempo de respuesta');

// for (let i = 0; i <= 10; i++) {
//     fs.readFileSync('archivo.txt', 'utf-8');
// }

for (let i = 0; i <= 10; i++) {
    const streamEscritura = fs.createReadStream('archivo.txt', {
        encoding: 'utf8'
    });
}

console.timeEnd('Tiempo de respuesta');

