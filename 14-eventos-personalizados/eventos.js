const fs = require('fs');
const streamEscritura = fs.createWriteStream('./archivos/original.txt');
const EventEmitter = require('events');

class Emisor extends EventEmitter {}

const miEmisor = new Emisor();

function escribirEnArchivo() {
    var iteraciones = 5;
    for (let i = 0; i < iteraciones; i++) {
        streamEscritura.write(`Iteracion #${i}\n`);        
    }
    streamEscritura.write(`========== FIN ==========`);
    streamEscritura.end();
}

function notificarPorCorreo() {
    console.log('preparando correo...');
    setTimeout(() => {
        miEmisor.emit('correoOk');
    }, 1000);
}

function leerDocumento() {
    fs.readFile('./archivos/original.txt', (error, documento) => {
        console.log(documento.toString());
    });
}
// escuchamos el evento close (viene del .end() del stream) para que
// luego que escriba todo, podamos leer el archivo
streamEscritura.on('close', () => {
    notificarPorCorreo();
});

miEmisor.on('correoOk', () => {
    leerDocumento();
});

escribirEnArchivo();